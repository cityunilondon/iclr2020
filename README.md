# Making DenseNet Interpretable: A Case Study in Clinical Radiology

### Description
This repository contains the associated code work for the interpretation framework discussed in the ICLR2020 paper. 

### Prerequisite
The framework has been tested with the following version of python, keras and tensorflow

* Python 3.6.8 
* Keras 2.2.4
* Tensorflow 1.13.1 (CUDA 10.0/libcuDNN 7.6.3)

### File Structure

| FileName | Description |
| ------ | ------ |
| `Model_Viz_Framework_ICLR2020.ipynb` | Jupyter Notebook for Interpretation Visualization Framework |
| saved_models | Folder for all pre-trained models |
| `best_MURA_model_ave_wrist.h5` | Pre-trained Model for Wrist Only Classification |
| valid_images | Folder for validation images |
| `XR_wrist-patient-11185-positive-image2.png` | Wrist X-Ray Image 2 for Patient 11185 Study 1 with Abnormality|


### Notes
* Due to license agreement, the MURA dataset is not included in the repository. Please request access from [Stanford ML Group](https://stanfordmlgroup.github.io/competitions/mura/ "MURA dataset").

